use std::ffi::CString;
use std::mem;

enum CrateTy {
    LIBC,
    NIX,
}

fn main() -> std::io::Result<()> {
    let path = "raw.txt";
    let use_type = CrateTy::LIBC;
    let s: libc::stat = match use_type {
        CrateTy::NIX => {
            println!("Using crate nix...");
            nix::sys::stat::stat(path).unwrap()
        }
        CrateTy::LIBC => {
            println!("Using raw crate libc...");
            use_libc_raw(path)
        }
    };

    println!("Stat info of file [{}] ---", path);
    println!("mode: {:#o}", s.st_mode);
    println!("ino: {}", s.st_ino);
    println!("nlink: {}", s.st_nlink);
    println!("uid: {}", s.st_uid);
    println!("gid: {}", s.st_gid);
    println!("size: {}", s.st_size);
    println!("blocks: {}", s.st_blocks);
    println!("atime: {}", s.st_atime);
    println!("mtime: {}", s.st_mtime);

    Ok(())
}

fn use_libc_raw(path: &str) -> libc::stat {
    let cstring = CString::new(path).unwrap();
    let mut uninittmp = mem::MaybeUninit::uninit();

    let r = unsafe { libc::stat(cstring.as_ptr(), uninittmp.as_mut_ptr()) };
    println!("libc::stat() retcode: {};", r);
    return unsafe { uninittmp.assume_init() };
}
