use std::{
    fs::{self, OpenOptions},
    io::{self, Write},
};

pub fn main() -> io::Result<()> {
    let mut f = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open("foo.txt")?;

    f.write_all(b"test1\n")?;
    f.write_all(b"test2\n")?;

    fs::write("tmp.cc", "my string")?;
    Ok(())
}