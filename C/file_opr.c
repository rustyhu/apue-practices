#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <wchar.h>

int main(void)
{
    const int BUFLEN = 1024; 
    // int fno = open("./tmp", O_RDWR);
    FILE *fp = fopen("./tmpfno", "r+");
    if (!fp)
    {
        printf("fopen failed! Recheck the file status.\n");
        exit(2);
    }
    fwide(fp, -1);

    char rbuf[BUFLEN];
    int ret = fread(rbuf, sizeof(char), 5, fp);
    printf("fread ret: [%d]\n", ret);

    int lastpos = ret < BUFLEN ? ret : BUFLEN-1;
    rbuf[lastpos] = '\0';
    printf("Get contents: [%s]\n", rbuf);

    return 0;
}
